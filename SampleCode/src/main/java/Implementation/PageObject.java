package Implementation;


import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.io.IOException;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofMillis;

public class PageObject {
    WebDriver driver;
    WebDriverWait mywait;
    int elementlocation;
    int swipesrequired;


    @FindBy(xpath = "//*[@id='root']/div/article/div/div[1]/div/ul/li[1]/a")
    private WebElement SignIn;

    @FindBy(xpath = "//*[@id='login']/div[2]/div/div/div/h3")
    private WebElement StartTrading;

    @FindBy(xpath = "//*[@id='login']/div[2]/div/div/div/div/div/form/div[1]/div/input")
    private WebElement Username;

    @FindBy(xpath = "//*[@id='login']/div[2]/div/div/div/div/div/form/div[2]/div/input")
    private WebElement Password;

    @FindBy(xpath = "//*[@id='login']/div[2]/div/div/div/div/div/form/div[3]/input")
    private WebElement TwoFactorAuthentication;

    @FindBy(xpath = "//*[@id='root']/div/header/div/div/button | //*[@id='root']/div/header/div/div/button/span")
    private WebElement Hamburger;

    @FindBy(xpath = "//*[@id='root']/div/div[2]/div/div[2]/button")
    private WebElement AcceptCookies;

    @FindBy(xpath = "//*[@id='root']/div/footer/div/div[2]/div[1]")
    private WebElement KrakenLogoBtmOfPge;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/span")
    private WebElement Features;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[1]/ul/li[1]/a")
    private WebElement Security;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[1]/ul/li[2]/a")
    private WebElement Liquidity;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[1]/ul/li[3]/a")
    private WebElement Fee_Structure;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[1]/ul/li[4]/a")
    private WebElement Support;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[1]/ul/li[5]/a")
    private WebElement Funding_Options;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[1]/a")
    private WebElement Margin_trading;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[2]/a")
    private WebElement Futures;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[3]/a")
    private WebElement OTC;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[4]/a")
    private WebElement Indices;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[5]/a")
    private WebElement Account_Management;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[1]/ul/li/div[2]/ul/li[6]/a")
    private WebElement Cryptowatch;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[3]/a")
    private WebElement Podcast;

    @FindBy(xpath = "//*[@id='root']/div/header/div/nav/ul[1]/li[5]/a")
    private WebElement Institutions;

    public PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void SignIn() {

        mywait = new WebDriverWait(driver, 30);
        mywait.until(ExpectedConditions.visibilityOf(AcceptCookies));
        AcceptCookies.click();

        mywait.until(ExpectedConditions.visibilityOf(SignIn));
        SignIn.click();

        mywait.until(ExpectedConditions.visibilityOf(StartTrading));
        verifyElementIsDisplayed(StartTrading);

        mywait.until(ExpectedConditions.visibilityOf(Username));
        Username.sendKeys("Earl Fernandes");

        mywait.until(ExpectedConditions.visibilityOf(Password));
        Password.sendKeys("Kraken1234");

        mywait.until(ExpectedConditions.visibilityOf(TwoFactorAuthentication));
        TwoFactorAuthentication.sendKeys("1234");

        driver.navigate().refresh();
        driver.navigate().back();
    }

    public void OpenandScrollMenuItems() {
        try {

            mywait = new WebDriverWait(driver, 30);

            WebElement[] Menuitemlist1 = {Security, Liquidity, Fee_Structure, Support, Funding_Options, Margin_trading, Futures, OTC, Indices};
            WebElement[] Menuitemlist2 = {Account_Management, Cryptowatch};
            WebElement[] Menuitemlist3 = {Podcast, Institutions};

            for (int i = 0; i < Menuitemlist1.length; i++) {

                mywait.until(ExpectedConditions.visibilityOf(Hamburger));
                Hamburger.click();
                mywait.until(ExpectedConditions.visibilityOf(Features));
                Features.click();
                mywait.until(ExpectedConditions.visibilityOf(Menuitemlist1[i]));
                Menuitemlist1[i].click();
                elementlocation = KrakenLogoBtmOfPge.getLocation().getY();
                System.out.println("Kraken Logo's Y coordinate at bottom of this page is: " + elementlocation);
                swipesrequired = (elementlocation / 750);
                System.out.println("Therefore swipes required approximately till bottom of page are: " + swipesrequired);
                SwipesRequired();
            }

            for (int i = 0; i < Menuitemlist2.length; i++) {

                mywait.until(ExpectedConditions.visibilityOf(Hamburger));
                Hamburger.click();
                mywait.until(ExpectedConditions.visibilityOf(Features));
                Features.click();
                ((AppiumDriver) driver).context("NATIVE_APP");
                verticalSwipeByPercentages(0.6, 0.3, 0.5);
                ((AppiumDriver) driver).context("CHROMIUM");
                mywait.until(ExpectedConditions.visibilityOf(Menuitemlist2[i]));
                Menuitemlist2[i].click();

                elementlocation = KrakenLogoBtmOfPge.getLocation().getY();
                System.out.println("Kraken Logo's Y coordinate at bottom of this page is: " + elementlocation);
                swipesrequired = (elementlocation / 750);
                System.out.println("Therefore swipes required approximately till bottom of page are: " + swipesrequired);
                SwipesRequired();
            }

            for (int i = 0; i < Menuitemlist3.length; i++) {

                mywait.until(ExpectedConditions.visibilityOf(Hamburger));
                Hamburger.click();
                mywait.until(ExpectedConditions.visibilityOf(Menuitemlist3[i]));
                Menuitemlist3[i].click();

                elementlocation = KrakenLogoBtmOfPge.getLocation().getY();
                System.out.println("Kraken Logo's Y coordinate at bottom of this page is: " + elementlocation);
                swipesrequired = (elementlocation / 750);
                System.out.println("Therefore swipes required approximately till bottom of page are: " + swipesrequired);
                SwipesRequired();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void SwipesRequired() {
        for (int j = 1; j < swipesrequired; j++) {
            ((AppiumDriver) driver).context("NATIVE_APP");
            verticalSwipeByPercentages(0.6, 0.3, 0.5);
            ((AppiumDriver) driver).context("CHROMIUM");
        }

        for (int k = 1; k < swipesrequired; k++) {
            ((AppiumDriver) driver).context("NATIVE_APP");
            verticalSwipeByPercentages(0.3, 0.6, 0.5);
            ((AppiumDriver) driver).context("CHROMIUM");
        }
    }

    public void verticalSwipeByPercentages(double startPercentage, double endPercentage, double anchorPercentage) {
        Dimension size = driver.manage().window().getSize();
        int anchor = (int) (size.width * anchorPercentage);
        int startPoint = (int) (size.height * startPercentage);
        int endPoint = (int) (size.height * endPercentage);

        new TouchAction((AppiumDriver) driver)
                .press(point(anchor, startPoint))
                .waitAction(waitOptions(ofMillis(1000)))
                .moveTo(point(anchor, endPoint))
                .release().perform();
    }

    public boolean verifyElementIsDisplayed(WebElement WebElement) {
		try {
			mywait = new WebDriverWait(driver, 30);
			mywait.until(ExpectedConditions.visibilityOf(WebElement));
			if (WebElement.isDisplayed()) {
				System.out.println("The element - " + " is displayed.");
				return true;
			} else {
				System.out.println("The element - " + " is not displayed");
				return false;
			}
		} catch (Exception e) {
			System.out.println("The element - " + " is not displayed");
			throw e;
		}
	}
}
