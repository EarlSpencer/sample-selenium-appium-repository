package Implementation;

import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.appium.java_client.android.AndroidDriver;

public class MobileTest {
	DesiredCapabilities caps = new DesiredCapabilities();
	public PageObject pgeObj;
	static WebDriver driver; 
	

	@BeforeTest

	public void baseClass() {
		try {
			

			caps.setCapability("deviceName", "Samsung");
			caps.setCapability("udid", "98897a433935385a4a");
			caps.setCapability("platformName", "Android");
			caps.setCapability("platformVersion", "9.0");
			caps.setCapability("browserName", "Chrome");
			caps.setCapability("noReset", "true");

			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
			driver.get("https://www.Kraken.com/");
			pgeObj = PageFactory.initElements(driver, PageObject.class);
			Thread.sleep(3000);

		} catch (Exception e) {
			e.getMessage();
			System.out.println(e);

		}
	}

	@Test // (dataProvider = "Authentication")
	public void MobileChallenge() throws Exception {
		try {
			pgeObj.SignIn();
			pgeObj.OpenandScrollMenuItems();
		} catch (Exception e) {
			System.out.println(e);
			//Assert.assertFalse(false, "FAIL");
		}
	}

	// @DataProvider

	/*
	 * public String[][] Authentication() throws Exception
	 * 
	 * {
	 * 
	 * // String[][] testObjArray = RBCUtils.getTableArray( //
	 * "/Users/mobileqa/git/rbc/RBC/src/main/java/testData/RBCData.xlsx",
	 * // "Sheet1");
	 * 
	 * return testObjArray;
	 * 
	 * }
	 */

	/*
	 * @AfterTest
	 * 
	 * public void tearDown() {
	 * 
	 * try
	 * 
	 * { driver.quit(); } catch (NullPointerException e) { e.getMessage(); }
	 * 
	 * 
	 * }
	 */

}
